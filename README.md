# dkatalis-test-jeri-pradita

dkatalis-test-jeri-pradita is automation code for dkatalist test

## Requirement And Installation

1. [Ruby](https://rvm.io/rvm/basics)
2. Selenium webdriver `gem install selenium-webdriver`
3. Cucumber `gem install cucumber`
4. bundler `gem install bundler`
5. install other dependencies `bundle install`
