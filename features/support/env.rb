require 'cucumber'
require 'selenium-webdriver'
require 'page-object'
require 'page-object/page_factory'
 
Before do
  # @browser = Selenium::WebDriver.for(:chrome)
  @browser = Selenium::WebDriver.for :chrome
end

World(PageObject::PageFactory)