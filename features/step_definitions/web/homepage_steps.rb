Given(/^user visit page "([^"]*)"$/) do |url|
  browser = HomePage.new(@browser)
  browser.visit_page(url)
end

Then(/^validate product "([^"]*)" displayed$/) do |product|
  browser = HomePage.new(@browser)
  browser.validate_product_display(product)
end