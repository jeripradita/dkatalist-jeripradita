require 'selenium-webdriver'
require 'rubygems'

class HomePage
  include PageObject

  def visit_page(url)
    get(url)
  end

  def validate_product_display(product)
    product_element = "//*[contains(text(),'#{product}')]"
    (Selenium::WebDriver::Wait.new :timeout => 10).until {
      find_element(xpath: product_element).displayed?
      find_element(xpath: "//*[contains(text(),'BUY NOW')]").displayed?
   }
  end

end